package birds;

import java.util.Comparator;

/**
 * This class generates different birds and displays the information to the console.
 *
 *  @author Write your UPI here
 */
public class BirdSanctuary {
    // An array of habitats for the wetland birds
    private final String[] HABITATS = {"swamps", "streams", "lagoons", "parks", "streams", "paddocks", "croplands", "parks", "swamps", "lagoons"};

    // This is the main method. Do not change this!
    public static void main(String[] args) {
        new BirdSanctuary().start();
    }

    // This is the start method. Do not change this!
    private void start() {
        System.out.println("Welcome to Auckland Bird Sanctuary. Here are our birds.");
        System.out.println("==========================");

        Bird[] nzBirds = createBirds();

        printAllBirds(nzBirds);

        System.out.println("==========================");
        System.out.println("Our wetland birds like to say hi!");
        System.out.println("-----------------------");

        printWetlandBirds(nzBirds);

        System.out.println("==========================");
        System.out.println("Our biggest bird likes to say hi too!");
        System.out.println("-----------------------");

        printBiggestBird(nzBirds);

        System.out.println("==========================");
        System.out.println("Thank you for visiting us :)");
    }

    //create the array and fill it with different birds
    private Bird[] createBirds(){
        Bird[] birds = new Bird[10];

        for (int i = 0; i < birds.length; i++) {
            int random = (int) (Math.random() * 100);
            if (random%3 == 0){
                birds[i] = new LittlePenguin(new Flying(), new Swimming());
            } else if (random%3 == 1){
                String habitat = HABITATS[(int)(Math.random()*10)];
                birds[i] = new Pukeko(habitat);
            } else {
                birds[i] = new Sparrow(new Flying(), new Drowning());
            }

        }
        return birds;
    }

    private void printAllBirds(Bird[] nzBirds){
        for (Bird nzBird : nzBirds) {
            System.out.println(nzBird.toString());
        }
    }

    private void printWetlandBirds(Bird[] nzBirds){
        for (Bird nzBird : nzBirds){
            if (nzBird instanceof WetlandBird){
                System.out.println(nzBird.greet("Cameron") +". My favourite places are " + ((WetlandBird) nzBird).favouritePlaces());
            }
        }
    }

    private void printBiggestBird(Bird[] nzBirds){
       Bird biggestBird = nzBirds[0];
        for (int i = 0; i < nzBirds.length; i++) {
            if (nzBirds[i].isBiggerThan(biggestBird)){
                biggestBird = nzBirds[i];
            }
        }
        System.out.printf("%s. I am the biggest bird with a random size of %.2f.%n", biggestBird.greet("Cameron"), biggestBird.size );
    }
}
