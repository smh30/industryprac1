package birds;

/**
 * This class defines a pukeko. A sparrow may fly or swim. It is a native New Zealand bird.
 *
 * @author Write your UPI here
 */
public class Pukeko extends Bird implements WetlandBird {
    private String habitat;

    public Pukeko(String habitat, IFly flyBehaviour, ISwim swimBehaviour) {
        super(flyBehaviour, swimBehaviour);
        this.name = "Pukeko";
        this.habitat = habitat;
    }


    public Pukeko(String habitat){
        super(new Flying(), new Swimming());
        this.name = "Pukeko";
        this.habitat = habitat;
    }

    @Override
    public String greet(String name) {
        return "Peki peki " + name;
    }


    @Override
    public boolean isNative() {
        return true;
    }

    @Override
    public String favouritePlaces() {
        return habitat;
    }
}
