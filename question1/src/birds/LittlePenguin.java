package birds;

public class LittlePenguin extends Bird {

    public LittlePenguin(IFly fly, ISwim swim){
        super(fly, swim);
        this.name = "Little Penguin";
    }

    @Override
    public String greet(String name) {
        return "Honk honk " + name;
    }

    @Override
    public boolean isNative() {
        return true;
    }
}
