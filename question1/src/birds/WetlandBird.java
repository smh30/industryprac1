package birds;

/**
 * This interface defines the WetlandBird type
 * Do not modify this interface!
 *
 * @author Yu-Cheng Tu
 */
public interface WetlandBird {
    /**
     * @return the favourite places of the wetland bird
     */
    public String favouritePlaces();
}
