package birds;

/**
 * This interface defines the behaviour of swimming.
 * Do not modify this interface!
 *
 * @author Yu-Cheng Tu
 */
public interface ISwim {

    /**
     * This method returns the swimming message.
     */
    public String swim();
}
