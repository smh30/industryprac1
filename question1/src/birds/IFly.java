package birds;

/**
 * This interface defines the behaviour of flying.
 * Do not modify this interface!
 *
 * @author Yu-Cheng Tu
 */
public interface IFly {

    /**
     * This method returns the flying message.
     */
    public String fly();
}
