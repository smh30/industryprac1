package musicstore;

import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This the application for running the music store.
 * TODO step b. - step f. modify this class appropriately
 *
 * @author Write your UPI here
 */
public class MusicStore {

    // This is the main method. Do not change this!
    public static void main(String[] args) {
        new MusicStore().start();
    }

    // This is the start method. Do not change this!
    private void start() {
        System.out.println("Welcome to the Auckland ICT Music Store");
        System.out.println("===============================");

        List<Album> albums = getAlbums("albums.csv");

        System.out.println("We have a total number of " + albums.size() + " albums available in store.");
        System.out.println("--------------------------------------------");
        System.out.println("Here is a quick summary of what we have in store for you :)");
        System.out.println("--------------------------------------------");

        System.out.println("Country albums: " + getTotalByGenre(albums, Genre.COUNTRY));
        System.out.println("Hip hop/rap albums: " + getTotalByGenre(albums, Genre.HIPHOP));
        System.out.println("Metal albums: " + getTotalByGenre(albums, Genre.METAL));
        System.out.println("Pop albums: " + getTotalByGenre(albums, Genre.POP));
        System.out.println("Rock albums: " + getTotalByGenre(albums, Genre.ROCK));
        System.out.println("Other albums: " + getTotalByGenre(albums, Genre.OTHER));

        System.out.println("--------------------------------------------");
        System.out.println("Exporting album information to a file was " +
                (exportAlbumsToFile(albums, "albumSummary1.txt") ? "successful!" : "not successful :("));

        System.out.println("--------------------------------------------");
        System.out.println("Latest 10 albums in store");
        System.out.println("--------------------------------------------");

        printLatestTenAlbums(albums);

        System.out.println("--------------------------------------------");
    }

    /**
     * Creates an album based on the given information.
     * Do not modify this method!
     *
     * @param albumInfo String array containing information about an album1
     * @return an album object
     */
    private Album createAlbum(String[] albumInfo) {
        int id = Integer.parseInt(albumInfo[0]);
        String album = albumInfo[1];
        Genre genre = getGenre(albumInfo[2]);
        String artist = albumInfo[3];
        int year = Integer.parseInt(albumInfo[4]);
        int tracks = Integer.parseInt(albumInfo[5]);
        int discs = Integer.parseInt(albumInfo[6]);
        return new Album(id, album, genre, artist, year, tracks, discs);
    }

    private Genre getGenre(String genre) {
        Genre albumGenre;
        switch (genre) {
            case "Country":
                albumGenre = Genre.COUNTRY;
                break;
            case "Hip Hop/Rap":
                albumGenre = Genre.HIPHOP;
                break;
            case "Metal":
                albumGenre = Genre.METAL;
                break;
            case "Pop":
                albumGenre = Genre.POP;
                break;
            case "Rock":
                albumGenre = Genre.ROCK;
                break;
            default:
                albumGenre = Genre.OTHER;
        }
        return albumGenre;
    }


    private List<Album> getAlbums(String filePath) {
        List<Album> allAlbums = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)))) {
            String currentAlbum;
            // to eat the first header line of the csv file
            reader.readLine();
            while ((currentAlbum = reader.readLine()) != null) {
                String[] albumDetails = currentAlbum.split(";");
                Album shinyNewAlbum = createAlbum(albumDetails);
                allAlbums.add(shinyNewAlbum);
            }

        } catch (IOException e) {
            System.out.println("An error occurred with the file reading: " + e);
        }

        return allAlbums;
    }

    private int getTotalByGenre(List<Album> albums, Genre genre) {
        int genreAlbums = 0;
        for (Album album : albums) {
            if (album.getGenre() == genre) {
                genreAlbums++;
            }
        }
        return genreAlbums;
    }


    private boolean exportAlbumsToFile(List<Album> albums, String filePath) {

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filePath)))) {
            for (Album album : albums) {
                writer.write(album.toString());
                writer.newLine();
            }
            return true;

        } catch (IOException e) {
            System.out.println("An error occurred with the file writing: " + e);
            return false;
        }

    }

    
    private void printLatestTenAlbums(List<Album> albums) {
        Collections.sort(albums);

        for (int i = 0; i < 10; i++) {
            System.out.printf("%s, \"%s\", %d%n", albums.get(i).getArtist(), albums.get(i).getTitle(), albums.get(i).getYear());
        }

    }


}
