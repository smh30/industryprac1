package musciapp;

import java.util.*;

/**
 * This the application for running the music app.
 * TODO step b. - step i. modify this class appropriately
 *  @author Write your UPI here
 */
public class MusicApp {
    // This is the main method. Do not change this!
    public static void main(String[] args) {
        new MusicApp().start();
    }

    // This is the start method. Do not change this!
    private void start() {
        System.out.println("Welcome to the 90's Music App.");
        int userChoice = -1;
        while (userChoice != 4) {
            System.out.println("===========================");
            mainMenu();
            userChoice = getUserMenuChoice();
            if (userChoice == 1) {
                System.out.println("Available Genres: Metal, Pop, Rap, Rock");
                String genre = getUserGenreChoice();
                Set<String> albumTitlesByGenre = getAlbumTitlesByGenre(AlbumCollection.ALBUMS, genre);
                System.out.println("-------------------------------------");
                System.out.println("All available " + genre.toLowerCase() + " albums:");
                System.out.println("-------------------");
                System.out.println(albumTitlesByGenre);
            }

            if (userChoice == 2) {
                System.out.println("Please enter any search terms: ");
                Set<Album> albums = findAlbumsByTitle(AlbumCollection.ALBUMS, Keyboard.readInput());
                System.out.println("-------------------------------------");
                if (albums.size() == 0) {
                    System.out.println("No albums found.");
                    continue;
                }
                System.out.println("Album(s) found:");
                System.out.println("-------------------");
                for (Album album : albums) {
                    System.out.println(album.getArtist() + ",  \"" + album.getTitle() + "\", " + album.getGenre() + ", " + album.getYear());
                }
            }

            if (userChoice == 3) {
                Map<String, Integer> artistsAndAlbums = processAlbums(AlbumCollection.ALBUMS);
                System.out.println("-------------------------------------");
                printAllArtists(artistsAndAlbums);
            }
        }
        System.out.println("Good bye! Please visit again!");
    }

    // This method displays the main menu. Do not change this!
    private void mainMenu() {
        System.out.println("1. Available Albums by Genre");
        System.out.println("2. Search Albums by Title");
        System.out.println("3. All Available Artists");
        System.out.println("4. Quit");
    }

    // there must be a more efficient way to do the second part but I guess this will work?
    private String validateGenre(String input) throws MusicGenreInvalidException {
        if (input.equals("")){
            throw new MusicGenreInvalidException("Empty Input!");
        } else if (!(input.trim().equalsIgnoreCase("POP")||input.trim().equalsIgnoreCase("ROCK")||input.trim().equalsIgnoreCase("METAL")||input.trim().equalsIgnoreCase("RAP"))){
            throw new MusicGenreInvalidException("Invalid Genre!");
        }
        return input;
    }


    private String getUserGenreChoice() {
        while (true) {
             System.out.print("Please enter a genre: ");
             try {
                 return validateGenre(Keyboard.readInput().trim());
             } catch (MusicGenreInvalidException m){
                 System.out.println(m);
             }
        }
    }


    private int validateUserInput(String input) throws MenuInvalidChoiceException {
        int userInput;
        if (input.equals("")){
            throw new MenuInvalidChoiceException("Empty Input!");
        } else {
            try {
                userInput = Integer.parseInt(input);
            } catch (NumberFormatException n){
                throw new MenuInvalidChoiceException("Invalid Number!");
            }
            if (userInput < 1 || userInput > 4){
                throw new MenuInvalidChoiceException("Invalid Number!");
            }
        }
        return userInput;
    }

    private int getUserMenuChoice() {
        while (true) {
            System.out.print("Please choose a number from the menu: ");
            try {
                return validateUserInput(Keyboard.readInput());
            } catch (MenuInvalidChoiceException m){
                System.out.println(m);
            }
        }
    }


    private Set<String> getAlbumTitlesByGenre(Album[] albums, String genre) {
        Set<String> genreAlbums = new HashSet<String>();
        for (Album album : albums) {
            if (album.getGenre().equalsIgnoreCase(genre)){
                genreAlbums.add(album.getTitle());
            }
        }
        return genreAlbums;
    }

    private Set<Album> findAlbumsByTitle(Album[] albums, String query) {
        Set<Album> titleAlbums = new HashSet<>();
        for (Album album : albums) {
            if (album.getTitle().toLowerCase().contains(query.toLowerCase())){
                titleAlbums.add(album);
            }
        }
        return titleAlbums;
    }

    private Map<String, Integer> processAlbums(Album[] albums) {
        Map<String, Integer> albumMap = new TreeMap<String, Integer>();
        for (Album album : albums) {
            // if the artist isn't already in the map, add them and 1 album
            if (!albumMap.containsKey(album.getArtist())){
                albumMap.put(album.getArtist(), 1);
            // if they're already in, increase the number of albums by 1
            } else {
                albumMap.put(album.getArtist(), (albumMap.get(album.getArtist())+1));
            }
        }
        return albumMap;
    }


    private void printAllArtists(Map<String, Integer> artistsAndAlbums) {
        for (String artist : artistsAndAlbums.keySet()){
            System.out.printf("Artist: %-25s %s %d%n", artist, "Albums produced: ", artistsAndAlbums.get(artist));
        }

    }

}
