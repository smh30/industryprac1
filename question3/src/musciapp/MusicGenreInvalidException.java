package musciapp;

public class MusicGenreInvalidException extends Exception {
    public MusicGenreInvalidException(String message) {
        super(message);
    }
}
